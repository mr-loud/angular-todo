import { Component, OnInit } from '@angular/core';

import { ModalService } from '../modal';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {
  todotasks: string[] = ['Clean the house', 'Go there', 'Buy some things'];

  constructor(private modalService: ModalService) { }

  ngOnInit() {
  }

  openModal(id: string): void {
    this.modalService.open(id);
  }

  closeModal(id: string): void {
    this.modalService.close(id);
  }
}
