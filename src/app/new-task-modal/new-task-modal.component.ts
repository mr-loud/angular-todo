import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-task-modal',
  templateUrl: './new-task-modal.component.html',
  styleUrls: ['./new-task-modal.component.css']
})
export class NewTaskModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
